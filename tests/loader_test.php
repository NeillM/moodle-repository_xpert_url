<?php
// This file is part of the Xpert URL download repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cache loader tests.
 *
 * @package    repository_xpert_url
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use repository_xpert_url\loader;
use repository_xpert_url\options;

/**
 * Tests the cache loader class.
 *
 * @package    repository_xpert_url
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group repository_xpert_url
 */
class repository_xpert_url_loader_testcase extends advanced_testcase {
    /**
     * Tests that a options object has no values in it.
     *
     * @param \repository_xpert_url\options $options
     */
    protected function assert_options_is_empty(options $options) {
        self::assertNull($options->author);
        self::assertNull($options->colours);
        self::assertNull($options->license);
        self::assertNull($options->size);
        self::assertNull($options->year);
        self::assertNull($options->url);
    }

    /**
     * Tests that The cache returns an empty options object when no entry is cached.
     */
    public function test_no_entry() {
        $loader = new loader();
        // Load an item that is unlikely to exist (since we have put nothing in).
        $options = $loader->get_options(5000);
        self::assertInstanceOf(options::class, $options);
        $this->assert_options_is_empty($options);
    }

    /**
     * Tests that we cache the values correctly.
     */
    public function test_cached() {
        $loader = new loader();
        $key = 69;

        // Create an option.
        $options = new options();
        $options->author = 'Some user';
        $options->url = 'https://example.com/';
        $options->colours = 'blackwhite';
        $options->license = 'cc';
        $options->year = '2019';
        $options->size = 'large';

        // Cache the options.
        $loader->save_options($key, $options);

        // Get the options from the cache.
        $cached = $loader->get_options($key);
        self::assertInstanceOf(options::class, $cached);
        self::assertNotSame($options, $cached);
        self::assertEquals($options, $cached);

        // Delete the entry.
        $loader->deletion_options($key);
        $cached2 = $loader->get_options($key);
        $this->assert_options_is_empty($cached2);
    }
}
