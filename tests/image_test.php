<?php
// This file is part of the Xpert URL download repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * URL downloader (Xpert) repository image creation and copyright attribution tests
 *
 * @package    repository_xpert_url
 * @copyright  2014 onwards, University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_url_image_testcase extends advanced_testcase {

    /**
     * Basic test of image attribution.
     *
     * @group repository_xpert_url
     * @group repository_xpert_url_image
     * @group uon
     * @return void
     */
    public function test_image() {
        $this->resetAfterTest(true);
        // Enable the repository.
        $this->getDataGenerator()->create_repository_type('xpert_url');
        // Test small image with small URL and small author.
        $this->check_small_image_with_short_text();
        // Test medium image with small URL and small author.
        $this->check_medium_image_with_short_text();
        // Test large image with small URL and small author.
        $this->check_large_image_with_short_text();
        // Test small image with small URL and long author name.
        $this->check_small_image_with_long_author();
        // Test medium image with small URL and long author name.
        $this->check_medium_image_with_long_author();
        // Test large image with small URL and long author name.
        $this->check_large_image_with_long_author();
        // Test small image with long URL and small author.
        $this->check_small_image_with_long_url();
        // Test large image with long URL and small author.
        $this->check_large_image_with_long_url();
        // Test small image with long URL and long author.
        $this->check_small_image_with_long_text();
        // Test medium image with long URL and long author.
        $this->check_medium_image_with_long_text();
        // Test large image with long URL and long author.
        $this->check_large_image_with_long_text();
    }

    /**
     * Check repository_xpert_url_image object for expected properties
     *
     * @param string $size
     * @param string $author
     * @param string $authorurl
     * @param string $datestamp
     * @param string $colourcombo
     * @param bool $extended
     * @param string $license
     * @return \repository_xpert_url_image
     */
    private function check_image($size, $author, $authorurl, $datestamp, $colourcombo, $extended = false, $license = 'cc') {
        $url = __DIR__ . "/fixtures/$size" . '_size.jpg';
        $year = '2014';
        $licensepath = __DIR__ . '/../pix/' . $size . '_' . $license . '.png';
        $image = new repository_xpert_url_image($url, $author, $authorurl, $license, $size, $year, $colourcombo);

        $this->check_text($image, $size);
        $this->check_size($image, $url, $licensepath, $extended);
        $this->check_colours($image, $colourcombo);
        return $image;
    }

    /**
     * Check a small image with short text
     */
    private function check_small_image_with_short_text() {
        $size = 'small';
        $author = 'Mr Test';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colourwhiteblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);

        $this->assertEquals(0, $image->textlines);
        $this->assertEquals(0, $image->urllines);
        $this->assertEquals(0, $image->urlchars->perline);
        $this->assertEquals(0, $image->urlchars->count);
        $this->assertEquals(0, $image->textchars->perline);
        $this->assertEquals(0, $image->textchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(197, $image->yposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);
        $this->assertEquals(1, $image->textfits);
        $this->assertEmpty($image->url);
        $this->assertEmpty($image->yearauthor);
        $this->assertStringContainsString("$datestamp - $author - www.test.com", $image->texttofit);
        $this->assertEquals($image::LAYOUT_ATTRIBUTION_BESIDE_LICENSE, $image->layout);

        $size = 'small';
        $author = 'Mr Test Little John';
        $authorurl = 'www.testmymediumurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourwhiteblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);

        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);
    }

    /**
     * Check a medium image with short text
     */
    private function check_medium_image_with_short_text() {
        $size = 'medium';
        $author = 'Mr Test';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(17, $image->textchars->count);
        $this->assertEquals(12, $image->urlchars->count);
        $this->assertEquals(88, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(32, $image->iconheight);
    }

    /**
     * Check a large image with short text
     */
    private function check_large_image_with_short_text() {
        $size = 'large';
        $author = 'Mr Test';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colourgreyblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(17, $image->textchars->count);
        $this->assertEquals(12, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(53, $image->iconheight);
    }

    /**
     * Check a small image with a long author name
     */
    private function check_small_image_with_long_author() {
        $size = 'small';
        $author = 'Mr Test with a really really really really long name';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colourbluebrown';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(2, $image->textlines);
        $this->assertEquals(0, $image->urllines);
        $this->assertEquals(62, $image->textchars->count);
        $this->assertEquals(0, $image->urlchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);

        $size = 'small';
        $author = 'Mr long Author name Author';
        $authorurl = 'www.ll.com';
        $datestamp = '2014';
        $colourcombo = 'colourwhiteblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(0, $image->urllines);
        $this->assertEquals(0, $image->urlchars->count);
        $this->assertEquals(49, $image->textchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);
    }

    /**
     * Check a medium size image with a long author name
     */
    private function check_medium_image_with_long_author() {
        $size = 'medium';
        $author = 'Mr Test with a really really really really long name';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colourpeachbrown';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(62, $image->textchars->count);
        $this->assertEquals(12, $image->urlchars->count);
        $this->assertEquals(88, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(32, $image->iconheight);
    }

    /**
     * Check a large image with a long author name
     */
    private function check_large_image_with_long_author() {
        $size = 'large';
        $author = 'Mr Test with a really really really really long name';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colouryellowblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(62, $image->textchars->count);
        $this->assertEquals(12, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(53, $image->iconheight);

        $author = 'Mr Test with a really really really really long name Mr Test with a really really really really long name Mr ' .
                'Test with a really really really really long name Mr Test with a really really really really long name Mr Test ' .
                'with a really really really really long name Mr Test with a really really really really long name';
        $size = 'large';
        $authorurl = 'www.test.com';
        $datestamp = '2014';
        $colourcombo = 'colouryellowblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(3, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(327, $image->textchars->count);
        $this->assertEquals(12, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(53, $image->iconheight);
    }

    /**
     * Check a small image with a long URL
     */
    private function check_small_image_with_long_url() {
        $size = 'small';
        $author = 'Mr Test';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourpinkblack';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(0, $image->textlines);
        $this->assertEquals(2, $image->urllines);
        $this->assertEquals(0, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);
    }

    /**
     * Check a large image with a long URL
     */
    private function check_large_image_with_long_url() {
        $size = 'large';
        $author = 'Mr Test';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(1, $image->textlines);
        $this->assertEquals(1, $image->urllines);
        $this->assertEquals(17, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(53, $image->iconheight);
        $this->assertEquals('www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com',
                $image->url);
    }

    /**
     * Check a small image with long author name & long URL
     */
    private function check_small_image_with_long_text() {
        $size = 'small';
        $author = 'Mr Test with a really really really really long name';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(2, $image->textlines);
        $this->assertEquals(2, $image->urllines);
        $this->assertEquals(62, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(16, $image->iconheight);

        $size = 'small';
        $author = 'Mr Test with a really really really really long name a really really really really long name a really really ' .
                'really really long name a really really really really long name';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(182, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(0, $image->xposition);
        $this->assertEquals(16, $image->iconheight);
    }

    /**
     * Check a small medium with long author name & long URL
     */
    private function check_medium_image_with_long_text() {
        $size = 'medium';
        $author = 'Mr Test with a really really really really long name Mr Test with a really really really really long name Mr ' .
                'Test with a really really really really long name Mr Test with a really really really really long name Mr Test ' .
                'with a really really really really long name Mr Test with a really really really really long name';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(327, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(88, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals(32, $image->iconheight);
    }

    /**
     * Check a large image with long author name & long URL
     */
    private function check_large_image_with_long_text() {
        $size = 'large';
        $author = 'Mr Test with a really really really really long name';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo);
        $this->assertEquals(62, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
        $this->assertEquals(0, $image->extralines);
        $this->assertEquals('www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com',
                $image->url);

        $author = 'Mr Test with a really really really really long name Mr Test with a really really really really long name Mr ' .
                'Test with a really really really really long name Mr Test with a really really really really long name Mr Test ' .
                'with a really really really really long name Mr Test with a really really really really long name';
        $size = 'large';
        $authorurl = 'www.testinglongurlreallylongurltestinglongurlreallylongurltestinglongurlreallylongurl.com';
        $datestamp = '2014';
        $colourcombo = 'colourblackwhite';
        $image = $this->check_image($size, $author, $authorurl, $datestamp, $colourcombo, true);
        $this->assertEquals(327, $image->textchars->count);
        $this->assertEquals(89, $image->urlchars->count);
        $this->assertEquals(150, $image->xposition);
    }

    /**
     * Check the font size, license and y baseline of an image
     *
     * @param repository_xpert_url_image $image
     * @param string $size
     */
    private function check_text($image, $size) {
        $this->assertEquals($size, $image->size);

        switch ($size) {
            case 'small':
                $this->assertEquals(7, $image->fontsize);
                $this->assertEquals(3, $image->ybaseline);
                break;
            case 'large':
                $this->assertEquals(12, $image->fontsize);
                $this->assertEquals(5, $image->ybaseline);
                break;
            case 'medium':
                $this->assertEquals(8, $image->fontsize);
                $this->assertEquals(3, $image->ybaseline);
        }
    }

    /**
     * Check the colour combination (background and font colour) of an attributed image
     *
     * @param repository_xpert_url_image $image
     * @param string $colourcombo
     * @return void
     */
    private function check_colours($image, $colourcombo) {
        switch ($colourcombo) {
            case 'colourwhiteblack':
                $this->assertEquals('0', $image->textredgreenblue['red']);
                $this->assertEquals('0', $image->textredgreenblue['green']);
                $this->assertEquals('0', $image->textredgreenblue['blue']);
                $this->assertEquals('255', $image->backgroundredgreenblue['red']);
                $this->assertEquals('255', $image->backgroundredgreenblue['green']);
                $this->assertEquals('255', $image->backgroundredgreenblue['blue']);
                break;
            case 'colourblackwhite':
                $this->assertEquals('255', $image->textredgreenblue['red']);
                $this->assertEquals('255', $image->textredgreenblue['green']);
                $this->assertEquals('255', $image->textredgreenblue['blue']);
                $this->assertEquals('0', $image->backgroundredgreenblue['red']);
                $this->assertEquals('0', $image->backgroundredgreenblue['green']);
                $this->assertEquals('0', $image->backgroundredgreenblue['blue']);
                break;
            case 'colourgreyblack':
                $this->assertEquals('0', $image->textredgreenblue['red']);
                $this->assertEquals('0', $image->textredgreenblue['green']);
                $this->assertEquals('0', $image->textredgreenblue['blue']);
                $this->assertEquals('204', $image->backgroundredgreenblue['red']);
                $this->assertEquals('204', $image->backgroundredgreenblue['green']);
                $this->assertEquals('204', $image->backgroundredgreenblue['blue']);
                break;
            case 'colourbluebrown':
                $this->assertEquals('35', $image->textredgreenblue['red']);
                $this->assertEquals('31', $image->textredgreenblue['green']);
                $this->assertEquals('32', $image->textredgreenblue['blue']);
                $this->assertEquals('197', $image->backgroundredgreenblue['red']);
                $this->assertEquals('239', $image->backgroundredgreenblue['green']);
                $this->assertEquals('253', $image->backgroundredgreenblue['blue']);
                break;
            case 'colourpeachbrown':
                $this->assertEquals('55', $image->textredgreenblue['red']);
                $this->assertEquals('47', $image->textredgreenblue['green']);
                $this->assertEquals('24', $image->textredgreenblue['blue']);
                $this->assertEquals('236', $image->backgroundredgreenblue['red']);
                $this->assertEquals('197', $image->backgroundredgreenblue['green']);
                $this->assertEquals('168', $image->backgroundredgreenblue['blue']);
                break;
            case 'colouryellowblack':
                $this->assertEquals('0', $image->textredgreenblue['red']);
                $this->assertEquals('0', $image->textredgreenblue['green']);
                $this->assertEquals('0', $image->textredgreenblue['blue']);
                $this->assertEquals('254', $image->backgroundredgreenblue['red']);
                $this->assertEquals('252', $image->backgroundredgreenblue['green']);
                $this->assertEquals('215', $image->backgroundredgreenblue['blue']);
                break;
            case 'colourpinkblack':
                $this->assertEquals('0', $image->textredgreenblue['red']);
                $this->assertEquals('0', $image->textredgreenblue['green']);
                $this->assertEquals('0', $image->textredgreenblue['blue']);
                $this->assertEquals('255', $image->backgroundredgreenblue['red']);
                $this->assertEquals('233', $image->backgroundredgreenblue['green']);
                $this->assertEquals('232', $image->backgroundredgreenblue['blue']);
                break;
            default:
                return;
        }
    }

    /**
     * Check the size of an attributed image
     *
     * @param repository_xpert_url_image $image
     * @param string $url
     * @param string $license
     * @param bool $extended
     */
    private function check_size($image, $url, $license, $extended = false) {
        $originalimage = imagecreatefromjpeg($url);
        $originalheight = imagesy($originalimage);
        $originalwidth = imagesx($originalimage);
        $height = imagesy($image->image);
        $width = imagesx($image->image);
        $licenseicon = imagecreatefrompng($license);
        $iconheight = imagesy($licenseicon) + 1;

        $this->assertEquals($originalwidth, $width);

        if ($extended) {
            // Because we know really long attribution text were passed, we expect to see an increased height of the overall image.
            // (Original image + attribution).
            // In this case we just make sure it is taller than the to total height of an image with shorter attribution text.
            $this->assertGreaterThan($originalheight + $iconheight, $height);
        } else {
            $this->assertEquals($originalheight + $iconheight, $height);
        }
    }
}
