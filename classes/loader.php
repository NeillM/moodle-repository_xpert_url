<?php
// This file is part of the Xpert URL download repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Loads data from the cache store for the repository.
 *
 * @package    repository_xpert_url
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace repository_xpert_url;

/**
 * Loads data from the cache store for the repository.
 *
 * @package    repository_xpert_url
 * @copyright  2020 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class loader implements \cache_data_source {
    /**
     * Returns the options cache.
     *
     * @return \cache_session
     */
    protected function get_data_cache(): \cache_session {
        return \cache::make('repository_xpert_url', 'options');
    }

    /**
     * Gets the options for a repository instance.
     *
     * @param int $key The repository instance id.
     * @return \repository_xpert_url\options
     */
    public function get_options(int $key): options {
        return $this->get_data_cache()->get($key);
    }

    /**
     * Saves the options.
     *
     * @param int $key The repository instance id.
     * @param \repository_xpert_url\options $options
     */
    public function save_options(int $key, options $options) {
        $this->get_data_cache()->set($key, $options);
    }

    /**
     * Deletes the options for a repository instance.
     *
     * @param int $key
     */
    public function deletion_options(int $key) {
        $this->get_data_cache()->delete($key);
    }

    /**
     * {@see \cache_data_source::get_instance_for_cache()}
     */
    public static function get_instance_for_cache(\cache_definition $definition) {
        return new loader();
    }

    /**
     * {@see \cache_data_source::load_for_cache()}
     */
    public function load_for_cache($key) {
        return new options();
    }

    /**
     * {@see \cache_data_source::load_many_for_cache()}
     */
    public function load_many_for_cache(array $keys) {
        $return = [];
        foreach ($keys as $key) {
            $return[$key] = $this->load_for_cache($key);
        }
        return $return;
    }
}
